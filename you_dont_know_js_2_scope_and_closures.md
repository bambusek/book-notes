# You don't know JS - Scope & Cluseres - book #2

Read [here](https://github.com/getify/You-Dont-Know-JS/blob/master/scope%20%26%20closures/ch1.md)

## CH#1 - What is Scope?

If compiler doesn't find a variable in the actual scope, it goes one level up till it reaches global scope.

- **LHS** - lookup a variable location, so it can be assigned something
- **RHS** - lookup variable and get its value

`ReferenceError` - when **LHS** lookup failed in all scopes( if `strict` mode is not on, copiler will define the variable for you)

`TypeError` - when doing something with variable we cannot do, like trying to execute non-function

## CH#2 - Lexical Scope

**Lexical scope** is used in most programming languages. It means that an object is in scope depending on were it is located in code. There is also **Dynamic scope**, used for example in Bash or some modes of Perl)

### Shadowing

Engine stops reference lookup after it finds the first reference to a variable. If there are two variables with same name in different levels of scope chain, the lower one shadows the upper one. 

### Cheating the scope

There are some ways how to alter the lexical scope, however it is not recommended to use them due to lowering program performance, possible side effects and because it it generally considered a bad practice.

#### Eval

`eval(...)` is a function that allows execution of a code passed as its argument. With `eval` we can alter the scope during the runtime.

```js
function foo(str, a) {
	eval( str ); // cheating!
	console.log( a, b );
}

var b = 2;

foo( "var b = 3;", 1 ); // 1 3
```

#### With

`with` is typically explained as a short-hand for making multiple property references against an object without repeating the object reference itself each time.

```js
var obj = {
	a: 1,
	b: 2,
	c: 3
};

// more "tedious" to repeat "obj"
obj.a = 2;
obj.b = 3;
obj.c = 4;

// "easier" short-hand
with (obj) {
	a = 3;
	b = 4;
	c = 5;
}
```

But it can be also used to set global scope:

```js
function foo(obj) {
	with (obj) {
		a = 2;
	}
}

var o1 = {
	a: 3
};

var o2 = {
	b: 3
};

foo( o1 );
console.log( o1.a ); // 2

foo( o2 );
console.log( o2.a ); // undefined
console.log( a ); // 2 -- Oops, leaked global!
```

## CH#3 - Function vs. Block Scope

By default JS has only global scope and a scope defined by function. 

### Hiding in Plain Scope

Function scope can be used to hide variables and other function, that we want to keep private and to avoid name collisions:

```js
function doSomething(a) {
	function doSomethingElse(a) {
		return a - 1;
	}
	var b;
	b = a + doSomethingElse( a * 2 );
	console.log( b * 3 );
}

doSomething( 2 ); // 15
```

### Block scope

Block scope can be created artifically using IIFE (which can be named function, for purposes of better debugging:

```js
(function foo(){
	var a = 3;
	console.log( a ); // 3
})();
```

ES6 brings `let` and `const` keywords that define block variable and constant.

## CH#4 Hoisting

In JS, all variable and function **declarations** are hoisted to the top of their scope, however **assignements** or any **other logic** is not!. Functions are hoisted first, then variables. Double declarations of variables are ignored. Double declarations of functions are overriden.

```js

// function hoisting and override
foo(); // 'Y'

function foo(){
    console.log('X');
}

function foo(){
    console.log('Y');
}

// another variable hoisting
b = 2;
var b;
console.log

// variable hoisting - declaration hoisted, assignement is not
console.log a; // 'undefined'
var a = 1;

```

While functions are hoisted, function expressions are not!

```js
foo(); // TypeError
bar(); // ReferenceError
var foo = function bar() {
	console.log('Z');
};

```

Also variables defined with `let` and `const` are *not* hoisted at all!
```js
{
    console.log(foo); // ReferenceError!
    console.log(bar); // ReferenceError!
    console.log(baz); // Undefined
    const foo = 1;
    let bar = 2;
    var baz = 3;
}