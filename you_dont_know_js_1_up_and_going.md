# You don't know JS - Up & Going - book #1

Read [here](https://github.com/getify/You-Dont-Know-JS/blob/master/up%20&%20going/README.md#you-dont-know-js-up--going)

## Chapter 2 - Into Javascript

### Values & Types

- `string`
- `number`
- `boolean`
- `null` and `undefined`
- `object`
 -`symbol` (new to ES6)
 
 **!!!**
 
 ```js
 var a = null 
 typeof a // "object" -- bug that is not going to be fixed
 
 var b = []
 typeof b // "object"
 
 var c = function(){return true}
 typeof c // "function" - subtype of "object"
```

### Comparing Values

**Coercion**

```js
// Explicit coercion
var x = "42";
var num = Number(x);

//Implicit coercion
var y = "42";
var num = y * 1 // "42" implicitly coerced to number
```

**Falsy values**

- `""` (empty string)
- `0`, `-0`, `NaN` (invalid number)
- `null`, `undefined`
- `false`

**Equality operators**

- `==` compares with allowed coercion (compares values)
- `===` comparec wit disallowed coercion (compares values and types)

### IIFE

Imediately invoked function expression

```js
(function(){
    console.log('Hello!');
})();
```

### Closure

A way to "remember" and continue to access a function's scope (its variables) even once the function has finished running.

```js
function makeAdder(x) {
	
    function add(y) {
        return y + x;
    };

    return add;
}

var plusOne = makeAdder( 1 );
var plusTen = makeAdder( 10 );

plusOne(3);		// 4  <-- 1 + 3
plusOne(41);	// 42 <-- 1 + 41
plusTen(13);	// 23 <-- 10 + 13
```



